import React from "react";
import HeroImage from "../assets/hero-image.jpg";

const HeroSection = () => {
  return (
    <div className="relative mb-20">
      <img
        src={HeroImage}
        alt=""
        className="w-full h-[400px] bg-cover filter brightness-75 transform -skew-y-6"
      />
      <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-white text-center text-4xl font-semibold">
        <p>Ideas</p>
        <p className="font-thin text-xl">where all our great things begin</p>
      </div>
    </div>
  );
};

export default HeroSection;
