import React, { useState, useEffect } from "react";
import { Link as ScrollLink, animateScroll as scroll } from "react-scroll";
import SuitMediaLogo from "../assets/suitmedia-logo.png";

const Navbar = () => {
  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.scrollY;
      setIsScrolled(scrollTop > 0);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div
      className={`flex items-center justify-around p-5 ${
        isScrolled ? "bg-orange-500 bg-opacity-90" : "bg-orange-500"
      } fixed top-0 left-0 right-0 z-50 transition-all duration-300 ease-in-out`}
    >
      <img src={SuitMediaLogo} alt="" width={150} />
      <div>
        <ul className="flex gap-x-8 text-white cursor-pointer">
          <li className="hover:text-black">Work</li>
          <li className="hover:text-black">About</li>
          <li className="hover:text-black">Services</li>
          <ScrollLink to="ideas">
            <li className="hover:text-black">Ideas</li>
          </ScrollLink>
          <li className="hover:text-black">Careers</li>
          <li className="hover:text-black">Contact</li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
