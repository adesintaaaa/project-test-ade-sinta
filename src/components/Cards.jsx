import React, { useState, useEffect } from "react";
import axios from "axios";
import { Card, Select, Pagination } from "antd";

const { Option } = Select;

const Cards = () => {
  const [ideas, setIdeas] = useState([]);
  const [sortBy, setSortBy] = useState(
    localStorage.getItem("sortBy") || "-created_at"
  );
  const [pageSize, setPageSize] = useState(
    localStorage.getItem("pageSize") || 10
  );
  const [currentPage, setCurrentPage] = useState(
    localStorage.getItem("currentPage") || 1
  );
  const apiUrl = `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${currentPage}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${sortBy}`;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(apiUrl, {
          headers: {
            "Content-Type": "application/json",
          },
        });

        if (response.status === 200) {
          setIdeas(response.data.data);
        } else {
          console.error("Invalid response status:", response.status);
        }
      } catch (error) {
        if (error.response && error.response.status === 429) {
          console.error("Rate limit exceeded. Please wait and try again.");
        } else {
          console.error("Error fetching data:", error);
        }
      }
    };

    fetchData();
  }, [apiUrl]);

  const handleSortChange = (value) => {
    setSortBy(value);
    setCurrentPage(1);
  };

  const handlePageSizeChange = (value) => {
    setPageSize(value);
    setCurrentPage(1);
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  useEffect(() => {
    localStorage.setItem("sortBy", sortBy);
  }, [sortBy]);

  useEffect(() => {
    localStorage.setItem("pageSize", pageSize);
  }, [pageSize]);

  useEffect(() => {
    localStorage.setItem("currentPage", currentPage);
  }, [currentPage]);

  return (
    <div className="p-10" id="ideas">
      <div className="flex justify-between mb-4">
        <div>
          <p>
            Showing {(currentPage - 1) * pageSize + 1} -{" "}
            {Math.min(currentPage * pageSize, 100)} Of 100
          </p>
        </div>
        <div className="flex items-center gap-x-10 pr-14">
          <div className="flex items-center space-x-4">
            <span>Show Per Page:</span>
            <Select value={pageSize} onChange={handlePageSizeChange}>
              <Option value={10}>10</Option>
              <Option value={20}>20</Option>
              <Option value={50}>50</Option>
            </Select>
          </div>
          <div className="flex items-center space-x-4 ">
            <span>Sort By:</span>
            <Select value={sortBy} onChange={handleSortChange}>
              <Option value="-created_at">Newest</Option>
              <Option value="created_at">Oldest</Option>
            </Select>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-4 gap-y-10">
        {ideas && ideas.length > 0 ? (
          ideas.map((idea) => (
            <Card
              key={idea.id}
              hoverable
              style={{ width: 400 }}
              cover={
                <img
                  alt={idea.title}
                  src={idea.small_image[0].url}
                  loading="lazy"
                  className="w-40 h-64"
                />
              }
            >
              <p>{idea.created_at}</p>
              <p className="clamp-3 font-semibold text-xl">{idea.title}</p>
            </Card>
          ))
        ) : (
          <p>No ideas found</p>
        )}
      </div>
      <div className="mt-10 flex flex-row justify-center">
        <Pagination
          current={currentPage}
          pageSize={pageSize}
          total={100}
          onChange={handlePageChange}
        />
      </div>
    </div>
  );
};

export default Cards;
