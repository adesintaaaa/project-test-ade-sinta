import React from 'react'
import Navbar from '../components/Navbar'
import HeroSection from '../components/HeroSection'
import Cards from '../components/Cards'

const LandingPage = () => {
  return (
    <div>
      <Navbar/>
      <HeroSection/>
      <Cards/>
    </div>
  )
}

export default LandingPage
